<?php
#Name:Scale Down Ritchey Image Data v1
#Description:Reduce the resolution of data conformed to Ritchey Image Data v1 by removing every second pixel, and removing the entire next row of pixels (eg: 4 pixels square becomes 1 pixel). Returns data as a string success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values. Image data must contain a field called "Colour", and it must have an RGB colour code as the value. Any other fields will not be included in the new data.
#Arguments:'string' is a string containing the data to increment. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,display_errors:bool:optional
#Content:
if (function_exists('scale_down_ritchey_image_data_v1') === FALSE){
function scale_down_ritchey_image_data_v1($string, $display_errors = NULL){
	$errors = array();
	$progress = '';
	if (@isset($string) === FALSE){
		$errors[] = "string";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		###Break into an array of lines, and delete every second line. If there is only one line then error.
		$data = @explode("\n", $string);
		if (@count($data) < 2){
			$errors[] = "vertical_pixels";
			goto result;
		}
		$switch = FALSE;
		foreach ($data as &$bit){
			if ($switch === FALSE){
				$switch = TRUE;
			} else {
				$switch = FALSE;
				$bit = '';
			}
		}
		unset($bit);
		$data = @array_filter($data);
		###Break each value (a line of pixels) into an array of pixels, and delete every second pixel. If there is only one pixel then return blank image.
		foreach ($data as &$bit){
			@preg_match_all("/Colour:.*?\./", $bit, $sub_bit, PREG_PATTERN_ORDER);
			$bit = $sub_bit[0];
			if (@count($bit) < 2){
				$errors[] = "horizontal_pixels";
				goto result;
			}
			$switch = FALSE;
			unset($sub_bit);
			foreach ($bit as &$sub_bit){
				if ($switch === FALSE){
					$switch = TRUE;
					$sub_bit = "[{$sub_bit}]";
				} else {
					$switch = FALSE;
					$sub_bit = '';
				}
			}
			unset($sub_bit);
			$bit = implode($bit);
		}
		$data = implode("\n", $data);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('scale_down_ritchey_image_data_v1_format_error') === FALSE){
				function scale_down_ritchey_image_data_v1_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("scale_down_ritchey_image_data_v1_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $data;
	} else {
		return FALSE;
	}
}
}
?>